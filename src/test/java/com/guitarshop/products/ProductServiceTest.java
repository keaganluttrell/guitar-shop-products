package com.guitarshop.products;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    ProductService productService;

    @Mock
    ProductRepository productRepository;

    List<Product> testProducts;

    @BeforeEach
    void setup() {
        productService = new ProductService(productRepository);
        testProducts = new ArrayList<>();
        String[] categories = new String[]{"guitars", "basses", "amps", "misc", "books"};
        Double[] prices = new Double[]{999.99, 799.99, 1199.99, 9.99, 19.99};
        for (int i = 0; i < 5; i++) {
            Product p = new Product("name" + i, "desc" + i, categories[i], prices[i]);
            testProducts.add(p);
        }
    }

    @Test
    void getProducts_Empty_returnsList() {
        when(productRepository.findAll()).thenReturn(testProducts);

        ProductsList productsList = productService.getProducts();
        assertNotNull(productsList);
        assertEquals(testProducts.size(), productsList.getProducts().size());
    }

    @Test
    void getProducts_Empty_returnsNull() {
        when(productRepository.findAll()).thenReturn(new ArrayList<>());
        ProductsList products = productService.getProducts();
        assertNull(products);
    }

    @Test
    void getProducts_Category_returnsList() {
        ArrayList<Product> foundItems = new ArrayList<>();
        foundItems.add(testProducts.get(0));
        when(productRepository.findByCategoryAndPriceLimit(anyString(), anyInt())).thenReturn(foundItems);
        ProductsList productsList = productService.getProducts("guitars", "0");
        assertNotNull(productsList);
        assertEquals(foundItems.size(), productsList.getProducts().size());
    }

    @Test
    void getProducts_Price_returnsList() {
        ArrayList<Product> foundItems = new ArrayList<>();
        foundItems.add(testProducts.get(3));
        foundItems.add(testProducts.get(4));
        when(productRepository.findByCategoryAndPriceLimit(anyString(), anyInt())).thenReturn(foundItems);
        ProductsList productsList = productService.getProducts("", "100");
        assertNotNull(productsList);
        assertEquals(foundItems.size(), productsList.getProducts().size());
    }

    @Test
    void getProducts_CategoryAndPrice_returnsList() {
        ArrayList<Product> foundItems = new ArrayList<>();
        foundItems.add(testProducts.get(2));
        when(productRepository.findByCategoryAndPriceLimit(anyString(), anyInt())).thenReturn(foundItems);
        ProductsList productsList = productService.getProducts("amps", "1500");
        assertNotNull(productsList);
        assertEquals(foundItems.size(), productsList.getProducts().size());
    }

    @Test
    void getProducts_CategoryAndPrice_returnsNull() {
        ArrayList<Product> foundItems = new ArrayList<>();

        when(productRepository.findByCategoryAndPriceLimit(anyString(), anyInt())).thenReturn(foundItems);
        ProductsList productsList = productService.getProducts("amps", "500");
        assertNull(productsList);
    }

    @Test
    void addProduct_Product_ReturnsProduct() {
        Product product = testProducts.get(0);

        when(productRepository.save(any(Product.class))).thenReturn(product);
        Product returnedProduct = productService.addProduct(product);

        assertNotNull(returnedProduct);
        assertEquals(product.getProductCode(), returnedProduct.getProductCode());
    }

    @Test
    void getProduct_ProductCode_ReturnsProduct() {
        Product product = testProducts.get(0);

        when(productRepository.findByProductCode(anyString())).thenReturn(product);
        Product returnedProduct = productService.getProduct(product.getProductCode().toString());

        assertNotNull(returnedProduct);
        assertEquals(product.getProductCode(), returnedProduct.getProductCode());
    }

    @Test
    void getProduct_ProductCode_ReturnsNull() {
        Product product = testProducts.get(0);

        when(productRepository.findByProductCode(anyString())).thenReturn(null);
        Product returnedProduct = productService.getProduct("NotFoundCode");

        assertNull(returnedProduct);
    }

    @Test
    void updateProduct_ProductCodeListPriceDiscountPercent_ReturnUpdatedProduct() {
        Product product = testProducts.get(0);
        product.setListPrice(499.99);
        product.setDiscountPercent(5);

        when(productRepository.findByProductCode(anyString())).thenReturn(product);
        when(productRepository.save(any(Product.class))).thenReturn(product);

        Product actual = productService.updateProduct(product.getProductCode().toString(), 499.99, 5);
        assertNotNull(actual);
        assertEquals(product.getListPrice(), actual.getListPrice());
        assertEquals(product.getDiscountPercent(), actual.getDiscountPercent());
    }

    @Test
    void updateProduct_ProductCodeListPriceDiscountPercent_ReturnNull() {
        when(productRepository.findByProductCode(anyString())).thenReturn(null);

        Product actual = productService.updateProduct("badProductCode", 499.99, 5);
        assertNull(actual);
    }

    @Test
    void deleteProduct_ProductCode_success() {
        Product product = testProducts.get(0);

        when(productRepository.findByProductCode(anyString())).thenReturn(product);
        productService.deleteProduct(product.getProductCode().toString());

        verify(productRepository).delete(any(Product.class));
    }
    @Test
    void deleteProduct_ProductCode_Exception() {
        when(productRepository.findByProductCode(anyString())).thenReturn(null);

        assertThrows(ProductNotFoundException.class, () -> {
            productService.deleteProduct("unknownProductCode");
        });

    }
}