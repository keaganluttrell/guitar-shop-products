package com.guitarshop.products;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductsController.class)
public class ProductsControllerTest {

    private final String URI = "/api/products";
    @Autowired
    MockMvc mockMvc;
    @MockBean
    ProductService productService;

    List<Product> products;

    @BeforeEach
    void setup() {
        products = new ArrayList<Product>();
        for (int i = 0; i < 5; i++) {
            Product product = new Product("les paul", "honey burst", "guitar", 999.99);
            products.add(product);
        }
    }

    private String toJSON(Object o) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(o);
    }


    // GET /api/products returns all autos 200
    @Test
    void getProducts_None_returnsAllProducts() throws Exception {

        when(productService.getProducts()).thenReturn(new ProductsList(products));

        mockMvc.perform(get(URI))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.products", hasSize(5)));
    }

    // GET /api/products returns 204 for no results
    @Test
    void getProducts_None_returnsNoContent() throws Exception {

        when(productService.getProducts()).thenReturn(null);

        mockMvc.perform(get(URI))
                .andExpect(status().isNoContent());
    }

    // GET /api/products?category=guitars
    @Test
    void getProducts_Category_returnsProductsInCategory() throws Exception {
        when(productService.getProducts(anyString(), anyString())).thenReturn(new ProductsList(products));

        mockMvc.perform(get(URI + "?category=guitars"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.products", hasSize(5)));
    }

    //     GET /api/products?price=1000
    @Test
    void getProducts_Price_returnsProductsInCategory() throws Exception {
        when(productService.getProducts(anyString(), anyString())).thenReturn(new ProductsList(products));

        mockMvc.perform(get(URI + "?price=1000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.products", hasSize(5)));
    }

    // GET /api/products?category=amps&price=1000
    @Test
    void getProducts_CategoryPrice_returnsProductsInCategory() throws Exception {
        when(productService.getProducts(anyString(), anyString())).thenReturn(new ProductsList(products));

        mockMvc.perform(get(URI + "?category=guitars&price=1000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.products", hasSize(5)));
    }

    // POST /api/products returns created product 200

    @Test
    void addProduct_Product_ReturnNewProduct() throws Exception {
        Product product = products.get(0);
        when(productService.addProduct(any(Product.class))).thenReturn(product);
        mockMvc.perform(post(URI, Product.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(product)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("productCode").value(product.getProductCode().toString()));
    }

    // POST /api/products return bad request 400
    @Test
    void addProduct_Product_BadRequest() throws Exception {
        Product product = products.get(0);
        when(productService.addProduct(any(Product.class))).thenReturn(product);
        mockMvc.perform(post(URI, Product.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(status().isBadRequest());
    }


    // GET api/products/{product_code} returns auto with matching vin 200

    @Test
    void getProduct_ProductCode_ReturnsProduct() throws Exception {
        Product product = products.get(0);
        when(productService.getProduct(anyString())).thenReturn(product);

        mockMvc.perform(get(URI + "/" + product.getProductCode()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("productCode").value(product.getProductCode().toString()));
    }

    // GET api/products/{product_code} returns no autos if not matching 204
    @Test
    void getProduct_ProductCode_NoContent() throws Exception {
        when(productService.getProduct(anyString())).thenReturn(null);

        mockMvc.perform(get(URI + "/noFoundProductCode"))
                .andExpect(status().isNoContent());
    }

    // PATCH api/products/{product_code} updates auto returns updated auto 200
    @Test
    void updateProduct_ProductCodeAndRequest_ReturnsUpdatedProduct() throws Exception {
        Product product = products.get(0);
        product.setDiscountPercent(3);
        product.setListPrice(99.99);
        when(productService.updateProduct(anyString(), anyDouble(), anyInt()))
                .thenReturn(product);
        ProductUpdateRequest body = new ProductUpdateRequest(99.99, 3);

        mockMvc.perform(patch(URI + "/" + product.getProductCode(), ProductUpdateRequest.class)
                .content(toJSON(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("productCode").value(product.getProductCode().toString()))
                .andExpect(jsonPath("listPrice").value(99.99))
                .andExpect(jsonPath("discountPercent").value(3));
    }

    // PATCH api/products/{product_code} returns no autos if not matching 204
    @Test
    void updateProduct_ProductCodeAndRequest_NoContent() throws Exception {
        Product product = products.get(0);
        product.setDiscountPercent(3);
        product.setListPrice(99.99);
        when(productService.updateProduct(anyString(), anyDouble(), anyInt()))
                .thenReturn(null);
        ProductUpdateRequest body = new ProductUpdateRequest(99.99, 3);

        mockMvc.perform(patch(URI + "/unknownProductCode", ProductUpdateRequest.class)
                .content(toJSON(body))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
    }

    // PATCH api/products/{product_code} returns bad request 400
    @Test
    void updateProduct_ProductCodeAndRequest_BadRequest() throws Exception {
        Product product = products.get(0);
        product.setDiscountPercent(3);
        product.setListPrice(99.99);
        when(productService.updateProduct(anyString(), anyDouble(), anyInt()))
                .thenReturn(product);
        ProductUpdateRequest body = new ProductUpdateRequest(99.99, 3);

        mockMvc.perform(patch(URI + "/" + product.getProductCode(), ProductUpdateRequest.class)
                .content(body.toString())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    // DELETE api/products/{product_code} updates auto returns updated auto 200

    @Test
    void deleteProduct_ProductCode_Accepted() throws Exception {
        Product product = products.get(0);

        mockMvc.perform(delete(URI + "/" + product.getProductCode()))
                .andExpect(status().isAccepted());

        verify(productService).deleteProduct(anyString());
    }

    // DELETE api/products/{products_code} returns no autos if not matching 204
    @Test
    void deleteProduct_ProductCode_NoContent() throws Exception {
        Product product = products.get(0);

        doThrow(new ProductNotFoundException()).when(productService).deleteProduct(anyString());
        mockMvc.perform(delete(URI + "/NotFoundProductCode"))
                .andExpect(status().isNoContent());
    }
}
