package com.guitarshop.products;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/products")
public class ProductsController {

    ProductService productService;

    public ProductsController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<ProductsList> getProducts(@RequestParam(required = false) String category,
                                                    @RequestParam(required = false) String price) {
        ProductsList products;
        if (category == null && price == null) {
            products = productService.getProducts();
        } else {
            if (category == null) category = "";
            if (price == null) price = "0";
            products = productService.getProducts(category, price);
        }
        if (products == null) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(products);
    }

    @PostMapping
    public Product addProduct(@RequestBody Product product) {
        return productService.addProduct(product);
    }

    @GetMapping("/{productCode}")
    public ResponseEntity<Product> getProduct(@PathVariable String productCode) {
        Product product = productService.getProduct(productCode);
        if (product == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(product);
    }

    @PatchMapping("/{productCode}")
    public ResponseEntity<Product> updateProduct(@PathVariable String productCode,
                                                 @RequestBody ProductUpdateRequest body) {
        double listPrice = body.getListPrice();
        int discountPercent = body.getDiscountPercent();
        Product updatedProduct = productService.updateProduct(productCode, listPrice, discountPercent);
        if (updatedProduct == null) return ResponseEntity.noContent().build();
        return ResponseEntity.ok(updatedProduct);
    }

    @DeleteMapping("/{productCode}")
    public ResponseEntity<?> deleteProduct(@PathVariable String productCode) {
        try {
            productService.deleteProduct(productCode);
        } catch (ProductNotFoundException e) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.accepted().build();
    }
}
