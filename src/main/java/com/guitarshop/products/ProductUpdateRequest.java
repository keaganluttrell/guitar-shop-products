package com.guitarshop.products;

public class ProductUpdateRequest {

    private final double listPrice;
    private final int discountPercent;

    public ProductUpdateRequest(double listPrice, int discountPercent) {
        this.listPrice = listPrice;
        this.discountPercent = discountPercent;
    }

    public double getListPrice() {
        return listPrice;
    }

    public int getDiscountPercent() {
        return discountPercent;
    }
}

