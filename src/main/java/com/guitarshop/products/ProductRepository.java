package com.guitarshop.products;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(nativeQuery = true, value = "select * from products where category = ? and price <= ?")
    List<Product> findByCategoryAndPriceLimit(String category, int price);

    //    @Query(nativeQuery = true, value = "select * from products where productCode = ?")
    Product findByProductCode(String productCode);
}
