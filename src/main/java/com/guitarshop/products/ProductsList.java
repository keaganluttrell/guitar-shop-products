package com.guitarshop.products;

import java.util.ArrayList;
import java.util.List;

public class ProductsList {

    public List<Product> products;

    public ProductsList(List<Product> products) {
        this.products = products;
    }

    public ProductsList() {
        this.products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public boolean empty() {
        return products.isEmpty();
    }
}
