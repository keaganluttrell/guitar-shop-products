package com.guitarshop.products;

import javax.persistence.Entity;
import java.util.Date;
import java.util.UUID;

@Entity
public class Product {

    private final UUID productCode;
    private final Date dateAdded;
    private String productName;
    private String description;
    private String category;
    private double listPrice;
    private double discountPercent;

    public Product(String productName, String description, String category, double listPrice) {
        this.productName = productName;
        this.description = description;
        this.category = category;
        this.listPrice = listPrice;
        this.productCode = UUID.randomUUID();
        this.dateAdded = new Date();
    }

    public UUID getProductCode() {
        return productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getListPrice() {
        return listPrice;
    }

    public void setListPrice(double listPrice) {
        this.listPrice = listPrice;
    }

    public double getDiscountPercent() {
        return discountPercent;
    }

    public void setDiscountPercent(double discountPercent) {
        this.discountPercent = discountPercent;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
