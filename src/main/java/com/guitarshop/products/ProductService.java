package com.guitarshop.products;

import org.springframework.stereotype.Service;

@Service
public class ProductService {

    ProductRepository productRepository;

    ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public ProductsList getProducts() {
        ProductsList productsList = new ProductsList(productRepository.findAll());
        if (productsList.empty()) {
            return null;
        }
        return productsList;
    }

    public ProductsList getProducts(String category, String price) {
        int priceInt = Integer.parseInt(price);
        if (priceInt == 0) priceInt = Integer.MAX_VALUE;
        ProductsList productsList = new ProductsList(productRepository.findByCategoryAndPriceLimit(category, priceInt));
        if (productsList.empty()) {
            return null;
        }
        return productsList;
    }

    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    public Product getProduct(String productCode) {
        return productRepository.findByProductCode(productCode);
    }

    public Product updateProduct(String productCode, double listPrice, int discountPercent) {
        Product foundProduct = productRepository.findByProductCode(productCode);
        if (foundProduct == null) return null;
        foundProduct.setListPrice(listPrice);
        foundProduct.setDiscountPercent(discountPercent);
        return productRepository.save(foundProduct);
    }

    public void deleteProduct(String productCode) {
        Product foundProduct = productRepository.findByProductCode(productCode);
        if (foundProduct == null) throw new ProductNotFoundException();
        else productRepository.delete(foundProduct);
    }
}
